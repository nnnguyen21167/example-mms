package com.example.kotlin

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class CustomAdapter(var context: Context, var arraylist: ArrayList<Monan>?) : BaseAdapter() {
    //
    class ViewHolder(row: View){
        var foodname: TextView
        var foodpicture: ImageView

        init {
            foodname = row.findViewById(R.id.txt_tenmonan) as TextView
            foodpicture = row.findViewById(R.id.img_hinhmonan) as ImageView
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view: View?
        var viewholder: ViewHolder
        if(convertView == null){
            var layoutinflater: LayoutInflater = LayoutInflater.from(context)
            view = layoutinflater.inflate(R.layout.list_item, null)
            viewholder = ViewHolder(view)
            view.tag = viewholder
        }else
        {
            view = convertView
            viewholder = convertView.tag as ViewHolder
        }

        var monan: Monan = getItem(position) as Monan
        viewholder.foodname.text = monan.nameF
        viewholder.foodpicture.setImageResource(monan.pictureF)
        return view as View
    }

    override fun getItem(position: Int): Any {
        return arraylist!!.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return arraylist?.size!!
    }
}