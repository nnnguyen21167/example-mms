package com.example.kotlin

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_kotlintest2.*

class Kotlintest2 : AppCompatActivity() {

    var arrayList : ArrayList<Monan> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlintest2)

        //set up value intent
        val myIntent = Intent(this, MainActivity::class.java)
        val message = txt_edit.text
        myIntent.putExtra("sendmessage",message)

        //btn back
        btn_btnback.setOnClickListener {
            if(myIntent.resolveActivity(packageManager) !=null) {
                startActivity(myIntent)
            }else{
                Log.d("Back","Fail!!!")
            }
        }


        var monan1: Monan = Monan("Cơm gà", R.drawable.monan1)
        var monan2: Monan = Monan("Cơm gà 2", R.drawable.monan2)
        var monan3: Monan = Monan("Cơm gà 3", R.drawable.monan3)

        arrayList.add(monan1)
        arrayList.add(monan2)
        arrayList.add(monan3)
        //btn add
        btn_btnadd.setOnClickListener { addList() }


    }


    fun addList() {
        lsv_list.adapter = CustomAdapter(this,arrayList)
    }
}
